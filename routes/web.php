<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['namespace' => 'App\Http\Controllers\Front'], function () {
    Route::group([
        'prefix' => "dashboard",
        "middleware" => ["permission:admin|agency.admin|agency.agent|agent"],
        "namespace" => "Dashboard"
    ], function () {
        Route::get('/', 'IndexController@index')->name('dashboard.index');
        Route::get('my-profile', 'IndexController@profile')->name('dashboard.profile');

        Route::group(['prefix' => "users", "middleware" => "role:admin"], function () {
            Route::get('/', 'UsersController@index')->name('dashboard.users');
            Route::get('{id}', 'UsersController@show')->name('dashboard.users.show');
        });
        Route::group(["prefix" => "aseguradora"], function () {
            Route::get('/', 'AseguradoraController@index')->name('dashboard.aseguradora.index');
            Route::get('create', 'AseguradoraController@create')->name('dashboard.aseguradora.create');
            Route::get('rangos', 'AseguradoraController@show')->name('dashboard.aseguradora.show');
            Route::get('polizas', 'AseguradoraController@show')->name('dashboard.aseguradora.show.polizas');
            Route::get('matriz', 'AseguradoraController@show')->name('dashboard.aseguradora.show.matriz');
        });
    });
    Route::group([
        'prefix' => '/',
        "namespace" => "Frontend"
    ], function(){
        Route::get('/','ResultsController@results')->name('index');
    });
});
Route::group(["namespace" => 'App\Http\Controllers\Back'], function () {
    Route::get('lang/{lang}', 'ToolsController@setLang')->name('tools.set.lang');
    Route::get('template/{template}', 'ToolsController@setTemplate')->name('tools.set.template');
    Route::get('menu-template/{template}', 'ToolsController@setMenuTemplate')->name('tools.set.menu_template');
    Route::get('timezone/{timezone}', 'ToolsController@setTimezone')->name('tools.set.timezone');
    Route::post('extend-session', 'ToolsController@extendSession')->name('extend.sessions');
});
