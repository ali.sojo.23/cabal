<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'namespace' => 'App\Http\Controllers\Back',
], function () {
    Route::group(['prefix' => 'users'], function () {
        Route::apiResource('information', 'InfoUserController')->except(['create', 'edit']);
        Route::apiResource('edit', 'UsersController')->except(['create', 'edit']);
        // Route::apiResource('verification','VerificationUsersController')->except(['create','edit']);
    });
    Route::group(['prefix' => 'files'], function () {
        Route::apiResource('/stored', 'FilesController')->only(['store', 'show', 'destroy']);
    });
    Route::group(["prefix" => 'aseguradora'], function () {
        Route::apiResource('data', 'AseguradoraController')->except(['create', 'edit']);
        Route::apiResource('rango', 'RangoEdadesController')->except(['create', 'edit']);
        Route::apiResource('polizas', 'RangoAlternativasController')->except(['create', 'edit']);
        Route::apiResource('matriz', 'MatrizPrecioController')->except(['create', 'edit']);
    });
    Route::get('search', 'QueryBuilderController@query');
});
