<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRangoEdadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rango_edades', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('aseguradora_id')->unsigned();
            $table->integer('min');
            $table->integer('max');
            $table->foreign('aseguradora_id')->on('aseguradoras')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rango_edades');
    }
}
