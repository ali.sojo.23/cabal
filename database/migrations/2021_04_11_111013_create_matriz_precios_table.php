<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatrizPreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriz_precios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rango_edades_id')->unsigned();
            $table->bigInteger('rango_alternativas_id')->unsigned();
            $table->bigInteger('precio');
            $table->foreign('rango_edades_id')->on('rango_edades')->references('id')->onDelete('cascade');
            $table->foreign('rango_alternativas_id')->on('rango_alternativas')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz_precios');
    }
}
