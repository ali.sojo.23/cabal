<?php

namespace App\Http\Controllers\Front\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    public function results(Request $r)
    {
        
        return view('frontend.results',[
            'title' => 'Cotización interactiva'
        ]);
    }
}
