<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class indexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $b = [
            __('custom/dashboard/index.breadcrumb.index')
        ];
        $t = __('custom/dashboard/index.title');
        $st = __('custom/dashboard/index.subtitle');
        return view('dashboard.index', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,

        ]);
    }
    public function profile()
    {
        $id = Auth::user();
        $b = [
            __('custom/dashboard/users.breadcrumb.users'),
            __('custom/dashboard/users.breadcrumb.admin')
        ];
        $t = __('custom/dashboard/users.title');
        $st = __('custom/dashboard/users.subtitle');

        return view('dashboard.users.show', [
            "user" => $id,
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,

            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=" . env('MIX_GOOGLE_MAPS_API_KEY') . "&libraries=places"
        ]);
    }
}
