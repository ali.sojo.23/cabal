<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Aseguradora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AseguradoraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $css = Cookie::get('template');
        if ($css === 'dark') {
            $this->css = 'css/dashboard/dark/main.css';
        } else {
            $this->css = 'css/dashboard/light/main.css';
        }
    }
    public function index()
    {
        $a = Aseguradora::all();
        $b = [
            __('custom/dashboard/aseguradora.breadcrumb.agencies'),
            __('custom/dashboard/aseguradora.breadcrumb.list')
        ];
        $t = __('custom/dashboard/aseguradora.title');
        $st = __('custom/dashboard/aseguradora.create.subtitle');

        return view('dashboard.aseguradora.index', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "aseguradora" => $a,
            'css' => $this->css
        ]);
    }
    public function create()
    {
        $b = [
            __('custom/dashboard/aseguradora.breadcrumb.agencies'),
            __('custom/dashboard/aseguradora.breadcrumb.create')
        ];
        $t = __('custom/dashboard/aseguradora.title');
        $st = __('custom/dashboard/aseguradora.create.subtitle');

        return view('dashboard.aseguradora.create', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            'css' => $this->css,
        ]);
    }
    public function show()
    {
        $a = Aseguradora::all();
        $b = [
            __('custom/dashboard/aseguradora.breadcrumb.agencies'),
            __('custom/dashboard/aseguradora.breadcrumb.create')
        ];
        $t = __('custom/dashboard/aseguradora.title');
        $st = __('custom/dashboard/aseguradora.create.subtitle');

        return view('dashboard.aseguradora.show', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            'css' => $this->css,
            'aseguradora' => $a
        ]);
    }
}
