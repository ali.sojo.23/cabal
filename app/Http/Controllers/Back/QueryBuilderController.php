<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\RangoEdades;
use Illuminate\Http\Request;

class QueryBuilderController extends Controller
{
    public function query(Request $r)
    {
        
        $q = RangoEdades::where('min', '<=', $r->edad)->where('max', '>=', $r->edad)->with(['aseguradora', 'matriz.alternativa'])->get();
        return $q;
    }
}
