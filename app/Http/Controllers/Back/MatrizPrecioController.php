<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;

use App\Models\MatrizPrecio;
use Illuminate\Http\Request;

class MatrizPrecioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request['rango_alternativas_id'] && $request['rango_edades_id']) :
            $precio = MatrizPrecio::where('rango_alternativas_id', $request['rango_alternativas_id'])->where('rango_edades_id', $request['rango_edades_id'])->get();
        endif;
        return $precio;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = [
            'rango_edades_id' => $request['rango_edades_id'],
            'rango_alternativas_id' => $request['rango_alternativas_id'],
            'precio' => $request['precio'],
        ];
        $search = [
            'rango_edades_id' => $request['rango_edades_id'],
            'rango_alternativas_id' => $request['rango_alternativas_id'],

        ];
        $matriz = MatrizPrecio::updateOrCreate($search, $save);

        return $matriz;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MatrizPrecio  $matrizPrecio
     * @return \Illuminate\Http\Response
     */
    public function show(MatrizPrecio $matrizPrecio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MatrizPrecio  $matrizPrecio
     * @return \Illuminate\Http\Response
     */
    public function edit(MatrizPrecio $matrizPrecio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MatrizPrecio  $matrizPrecio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MatrizPrecio $matrizPrecio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MatrizPrecio  $matrizPrecio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MatrizPrecio $matrizPrecio)
    {
        //
    }
}
