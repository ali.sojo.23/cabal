<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Aseguradora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AseguradoraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = asset('img/logo.png');
        if ($request['logo']) {
            $file = str_replace(env('APP_URL') . '/storage/', '', $request['logo']);

            $path = str_replace('temp/', '', $file);

            $file = Storage::disk('public')->move($file, 'agency/' . $path);
            if ($file == true) {
                $file = asset('storage/agency/' . $path);
            }
        }
        $a = Aseguradora::create([
            "nombre" => $request->nombre,
            "logo" => $file
        ]);
        return response($a, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aseguradora  $aseguradora
     * @return \Illuminate\Http\Response
     */
    public function show(Aseguradora $aseguradora)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Aseguradora  $aseguradora
     * @return \Illuminate\Http\Response
     */
    public function edit(Aseguradora $aseguradora)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aseguradora  $aseguradora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aseguradora $aseguradora)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aseguradora  $aseguradora
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aseguradora $aseguradora)
    {
        //
    }
}
