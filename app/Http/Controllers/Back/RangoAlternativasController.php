<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;

use App\Models\RangoAlternativas;
use Illuminate\Http\Request;

class RangoAlternativasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $a = null;
        if ($request->aseguradora) :
            $a = RangoAlternativas::where('aseguradora_id', $request->aseguradora)->orderBy('amount')->get();
        endif;
        return $a;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r = RangoAlternativas::create([
            "aseguradora_id" => $request['aseguradora_id'],
            "plan_name" => $request['plan_name'],
            "amount" => $request['amount'],
            "maternidad" => $request['maternidad']
        ]);

        return response($r, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RangoAlternativas  $rangoAlternativas
     * @return \Illuminate\Http\Response
     */
    public function show(RangoAlternativas $rangoAlternativas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RangoAlternativas  $rangoAlternativas
     * @return \Illuminate\Http\Response
     */
    public function edit(RangoAlternativas $rangoAlternativas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RangoAlternativas  $rangoAlternativas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RangoAlternativas $poliza)
    {
        if ($poliza['id'] == $request['id']) {
            $poliza['plan_name'] = $request['plan_name'];
            $poliza['amount'] = $request['amount'];
            $poliza["maternidad"] = $request['maternidad'];
            $poliza->save();
            return response($poliza, 200);
        } else {
            return response('error', 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RangoAlternativas  $rangoAlternativas
     * @return \Illuminate\Http\Response
     */
    public function destroy(RangoAlternativas $poliza)
    {
        $poliza->delete();
        return true;
    }
}
