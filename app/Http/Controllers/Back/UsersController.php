<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Mail\WelcomeUserEmail;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch ($request['withPermission']):
            case ('admin'):
                $u = User::role('admin')->get();
                break;
            default:
                $type = $request['withPermission'];
                $u = User::whereHas('permissions', function ($q) use ($type) {
                    $q->where('name', $type);
                })->get();
                break;
        endswitch;
        if (!$request['withPermission']) {
            $u = User::all();
        }
        return $u;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $file = asset('img/logo.png');
        $u = User::where('email', $request['email'])->first();
        if ($u) {
            return response(['msg' => __('custom/dashboard/users.create.response.error.email_exist')], 400);
        } else {
            if ($request['feature_image']) {
                $file = str_replace(env('APP_URL') . '/storage/', '', $request['feature_image']);
                $path = str_replace('image/', '', $file);

                $file = Storage::disk('public')->move($file, 'user/' . $path);
                if ($file == true) {
                    $file = asset('storage/user/' . $path);
                }
            }
            $token = encrypt(Str::random(10) . Date('U'));
            $u = User::create([
                "firstname" => $request['firstname'],
                "lastname" => $request['lastname'],
                "email" => $request['email'],
                "role" => $request['role'] ?? 'user',
                "password" => $token,
                "feature_image" => $file
            ]);
            PasswordReset::create([
                "email" => $u->email,
                "token" => $token
            ]);

            Mail::to($u->email)->send(new WelcomeUserEmail($u, $token));

            return $u;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request['type']) {
            $u = User::where($request['type'], $id)->first();
        } else {
            $u = User::find($id);
        }

        if ($u) {
            return $u;
        } else {
            return response(['msg' => __('custom/dashboard/users.create.response.error.email_nonexist')], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $u = User::find($id);
        if ($u) :

            if ($request['newpassword'] && $request['oldpassword']) :
                $validator = Validator::make($request->toArray(), [
                    'newpassword' => ['required', 'string', 'min:8'],
                ]);
                if (!$validator->fails()) {

                    $pass = Hash::check($request['oldpassword'], $u->password);
                    if ($pass) {
                        $u->password = Hash::make($request['newpassword']);
                        $u->save();
                        return response(['message' => __('custom/dashboard/users.profile.response.success.password_reseted')], 200);
                    } else {
                        return response(['message' => __('custom/dashboard/users.profile.response.error.missmatch_pass')], 409);
                    }
                } else {
                    return response(['message' => __('dashboard/profile.response.error.newpassword_no_validate')], 409);
                }

            endif;
        else :
            return response(['msg' => __('custom/dashboard/users.create.response.error.email_nonexist')], 400);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
