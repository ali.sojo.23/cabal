<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;

use App\Models\RangoEdades;
use Illuminate\Http\Request;

class RangoEdadesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $a = null;
        if ($request->aseguradora) :
            $a = RangoEdades::where('aseguradora_id', $request->aseguradora)->with('matriz')->orderBy('min')->get();
        endif;
        return $a;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r = RangoEdades::create([
            "aseguradora_id" => $request['aseguradora_id'],
            "min" => $request['min'],
            "max" => $request['max']
        ]);
        return $r;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RangoEdades  $rangoEdades
     * @return \Illuminate\Http\Response
     */
    public function show(RangoEdades $rangoEdades)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RangoEdades  $rangoEdades
     * @return \Illuminate\Http\Response
     */
    public function edit(RangoEdades $rangoEdades)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RangoEdades  $rangoEdades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RangoEdades $rango)
    {
        if ($rango->id == $request['id']) :
            $rango->min = $request->min;
            $rango->max = $request->max;
            $rango->save();
            return response($rango, 200);
        else :
            return response('error', 403);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RangoEdades  $rangoEdades
     * @return \Illuminate\Http\Response
     */
    public function destroy(RangoEdades $rango)
    {
        $rango->delete();
        return true;
    }
}
