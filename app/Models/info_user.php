<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class info_user extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'country',
        'ciudad',
        'address_1',
        'address_2',
        'phone',
        'description',
        'postal_code',
        'dni',
        'gender',
        'birthdate'
    ];

    protected function pais(){
        return $this->hasOne('App\Models\countries','id','country');
    }
    protected function estado(){
        return $this->hasOne('App\Models\states','id','ciudad');
    }
    
}
