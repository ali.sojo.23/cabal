<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RangoEdades extends Model
{
    use HasFactory;

    protected $fillable = [
        'aseguradora_id',
        'min',
        'max',
    ];

    public function matriz()
    {
        return $this->hasMany(MatrizPrecio::class, 'rango_edades_id', 'id');
    }
    public function aseguradora()
    {
        return $this->hasOne(Aseguradora::class, 'id', 'aseguradora_id');
    }
}
