<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RangoAlternativas extends Model
{
    use HasFactory;
    protected $fillable = [
        'aseguradora_id',
        'plan_name',
        'amount',
        "maternidad"
    ];
    public function aseguradora()
    {
        return $this->hasOne(Aseguradora::class, 'id', 'aseguradora_id');
    }
}
