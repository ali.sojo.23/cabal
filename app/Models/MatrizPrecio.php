<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatrizPrecio extends Model
{
    use HasFactory;
    protected $fillable = [
        'rango_edades_id',
        'rango_alternativas_id',
        'precio',
    ];
    public function alternativa()
    {
        return $this->hasOne(RangoAlternativas::class, 'id', 'rango_alternativas_id');
    }
}
