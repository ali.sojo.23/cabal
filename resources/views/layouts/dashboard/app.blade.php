<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <link
            href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,200,300,400,500,700"
            rel="stylesheet"
        />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{$googleApi ?? ''}}"></script> 
    <link rel="stylesheet" href="{{asset('css/dashboard/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/color_skins.css')}}">
    @php
        $css = Cookie::get('template');
        if ($css == 'dark') {
            $css = 'css/dashboard/dark/main.css';
        } else {
            $css = 'css/dashboard/light/main.css';
        }
    @endphp
    <link rel="stylesheet" href="{{asset($css)}}"> 
    <link rel="stylesheet" href="{{asset('css/dashboard/app.css?version='.env('APP_VERSION'))}}"> 
    
    
</head>
@php
    $menu = Cookie::get('menu_template');
@endphp
<body class="theme-purple @if($menu ==='dark') menu_dark @endif">
    <div id="main">
        @include('layouts.dashboard.assets.loader')
        <div class="overlay"></div>
        @include('layouts.dashboard.assets.topBar')
        @include('layouts.dashboard.assets.leftSidebar')
        @include('layouts.dashboard.assets.rightSidebar')
        <section id="main" class="content">
            @include('layouts.dashboard.assets.breadcrumBar')
            @yield('content')
        </section>
        @include('layouts.dashboard.assets.modal')
    </div>
    
    <script defer src="{{asset('js/dashboard/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
    <script defer src="{{asset('js/dashboard/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
    <script defer src="{{asset('js/dashboard/mainscripts.bundle.js')}}"></script> 
    <script src="{{asset('js/app.js?version='.env('APP_VERSION'))}}"></script>
</body>
</html>
