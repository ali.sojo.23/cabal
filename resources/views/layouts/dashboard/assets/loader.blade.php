<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="" src="/img/logo.png" height="100" alt="{{config('app.name','')}}"></div>
        <p>@lang('custom/dashboard/index.loader')</p>        
    </div>
</div>