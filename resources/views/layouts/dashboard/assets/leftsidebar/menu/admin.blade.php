<li class="header">{{ __("custom/dashboard/menu.administration") }}</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-account"></i
        ><span>@lang('custom/dashboard/menu.admin.users')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.users') }}"
                >@lang('custom/dashboard/menu.admin.users_admin')</a
            >
        </li>
    </ul>
</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-home"></i
        ><span>@lang('custom/dashboard/menu.admin.aseguradora')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.aseguradora.index')}}"
                >@lang('custom/dashboard/menu.admin.aseguradora_index')</a
            >
        </li>
        <li class="menu-item">
            <a href="{{ route('dashboard.aseguradora.create')}}"
                >@lang('custom/dashboard/menu.admin.aseguradora_create')</a
            >
        </li>
    </ul>
</li>
<li class="header">{{ __("custom/dashboard/menu.admin.aseguradora_config") }}</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-tag"></i>
        <span>@lang('custom/dashboard/menu.admin.rangos')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{route('dashboard.aseguradora.show')}}"
                >@lang('custom/dashboard/menu.admin.show_ranges')</a
            >
        </li>
        <li class="menu-item">
            <a href="{{route('dashboard.aseguradora.show.polizas')}}"
                >@lang('custom/dashboard/menu.admin.create_ranges')</a
            >
        </li>
    </ul>
</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-tag"></i>
        <span>@lang('custom/dashboard/menu.admin.aseguradora_admin')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{route('dashboard.aseguradora.show.matriz')}}"
                >@lang('custom/dashboard/menu.admin.set_price')</a
            >
        </li>
    </ul>
</li>
