<div class="modal fade" id="defaultModal" tabindex="-1" data-backdrop="false" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title text-center" id="defaultModalLabel">@lang('custom/dashboard/index.modal.extend_sessions')</h4>
            </div>
            <div class="modal-body"> @lang('custom/dashboard/index.modal.extend_sessions_message')</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" v-on:click.prevent="closeModal()"><strong>@lang('custom/dashboard/index.modal.button.cancel')</strong></button>
                <button type="button" class="btn btn-primary waves-effect" v-on:click.prevent="extendSession()"><strong>@lang('custom/dashboard/index.modal.button.continue')</strong></button>
                <form
                action="{{ route('extend.sessions') }}"
                id="extendSessionForm"
                method="POST"
                class="d-none"
            >
                @csrf
            </form>
            </div>
        </div>
    </div>
</div>