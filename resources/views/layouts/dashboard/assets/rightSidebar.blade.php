@php
    $template = Cookie::get('template');
    $lang = Cookie::get('lang') ?? 'es';
@endphp
<aside id="rightsidebar" class="right-sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activity">Activity</a></li> --}}
    </ul>
    <div class="tab-content">
        <div class="tab-pane slideRight active" id="setting">
            <div class="slim_scroll">
                {{-- <div class="card theme-light-dark">
                    <h6>@lang('custom/dashboard/menu.right_sidebar.languaje')</h6>
                    <a href="{{route('tools.set.lang','en')}}" class="btn btn-default btn-block @if($lang == 'en') @if($template == 'dark') btn-simple @endif @else  @if($template != 'dark') btn-simple  @endif @endif btn-round">@lang('custom/dashboard/menu.right_sidebar.lang.english')</a>
                    <a href="{{route('tools.set.lang','es')}}" class="btn btn-default btn-block  @if($lang == 'es') @if($template == 'dark') btn-simple @endif @else  @if($template != 'dark') btn-simple  @endif @endif btn-round">@lang('custom/dashboard/menu.right_sidebar.lang.spanish')</a>
                    <a href="{{route('tools.set.lang','pt_BR')}}" class="btn btn-default btn-block @if($lang == 'pt_BR') @if($template == 'dark') btn-simple @endif @else  @if($template != 'dark') btn-simple  @endif @endif btn-round">@lang('custom/dashboard/menu.right_sidebar.lang.portuguese')</a>                 
                </div>  --}}
                <div class="card theme-light-dark">
                    <h6>@lang('custom/dashboard/menu.right_sidebar.template')</h6>
                    <a href="{{route('tools.set.template','light')}}" class="btn btn-default btn-simple btn-round btn-block">@lang('custom/dashboard/menu.right_sidebar.light')</a>
                    <a href="{{route('tools.set.template','dark')}}" class="btn btn-default btn-round btn-block">@lang('custom/dashboard/menu.right_sidebar.dark')</a>                 
                </div> 
                @if ($template != 'dark')
                <div class="card theme-light-dark">
                    <h6>@lang('custom/dashboard/menu.right_sidebar.left_menu_template')</h6>
                    <a href="{{route('tools.set.menu_template','light')}}" class="t-light btn btn-default btn-simple btn-round btn-block">@lang('custom/dashboard/menu.right_sidebar.light')</a>
                    <a href="{{route('tools.set.menu_template','dark')}}" class="t-dark btn btn-default btn-round btn-block">@lang('custom/dashboard/menu.right_sidebar.dark')</a>                    
                </div>                
                
                @endif
            </div>                
        </div>       
        {{-- <div class="tab-pane slideLeft" id="activity">
            <div class="slim_scroll">
                <div class="card user_activity">
                    <h6>Recent Activity</h6>
                    <div class="streamline b-accent">
                        <div class="sl-item">
                            <img class="user rounded-circle" src="..\assets\images\xs\avatar4.jpg" alt="">
                            <div class="sl-content">
                                <h5 class="m-b-0">Admin Birthday</h5>
                                <small>Jan 21 <a href="javascript:void(0);" class="text-info">Sophia</a>.</small>
                            </div>
                        </div>
                        <div class="sl-item">
                            <img class="user rounded-circle" src="..\assets\images\xs\avatar5.jpg" alt="">
                            <div class="sl-content">
                                <h5 class="m-b-0">Add New Contact</h5>
                                <small>30min ago <a href="javascript:void(0);">Alexander</a>.</small>
                                <small><strong>P:</strong> +264-625-2323</small>
                                <small><strong>E:</strong> maryamamiri@gmail.com</small>
                            </div>
                        </div>
                        <div class="sl-item">
                            <img class="user rounded-circle" src="..\assets\images\xs\avatar6.jpg" alt="">
                            <div class="sl-content">
                                <h5 class="m-b-0">Code Change</h5>
                                <small>Today <a href="javascript:void(0);">Grayson</a>.</small>
                                <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                            </div>
                        </div>
                        <div class="sl-item">
                            <img class="user rounded-circle" src="..\assets\images\xs\avatar7.jpg" alt="">
                            <div class="sl-content">
                                <h5 class="m-b-0">New Email</h5>
                                <small>45min ago <a href="javascript:void(0);" class="text-info">Fidel Tonn</a>.</small>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="card">
                    <h6>Recent Attachments</h6>
                    <ul class="list-unstyled activity">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-collection-pdf l-blush"></i>                    
                                <div class="info">
                                    <h4>info_258.pdf</h4>                    
                                    <small>2MB</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-collection-text l-amber"></i>                    
                                <div class="info">
                                    <h4>newdoc_214.doc</h4>                    
                                    <small>900KB</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-image l-parpl"></i>                    
                                <div class="info">
                                    <h4>MG_4145.jpg</h4>                    
                                    <small>5.6MB</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-image l-parpl"></i>                    
                                <div class="info">
                                    <h4>MG_4100.jpg</h4>                    
                                    <small>5MB</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-collection-text l-amber"></i>                    
                                <div class="info">
                                    <h4>Reports_end.doc</h4>                    
                                    <small>780KB</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-videocam l-turquoise"></i>                    
                                <div class="info">
                                    <h4>movie2018.MKV</h4>                    
                                    <small>750MB</small>
                                </div>
                            </a>
                        </li>                        
                    </ul>
                </div>
            </div>
        </div> --}}
    </div>
</aside>