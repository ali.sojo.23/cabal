<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            @include('layouts.dashboard.assets.leftsidebar.userInfo')
                        </div>
                    </li>
                    <li class="header">{{__('custom/dashboard/menu.index')}}</li>
                    <li class="menu-item">
                        <a href="{{route('dashboard.index')}}"
                            ><i class="zmdi zmdi-chart"></i><span>@lang('custom/dashboard/menu.dashboard')</span>
                        </a>
                    </li>
                    @include('layouts.dashboard.assets.leftsidebar.menu')
                </ul>
            </div>
        </div>
        
    </div>    
</aside>