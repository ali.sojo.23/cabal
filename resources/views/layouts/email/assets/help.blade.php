<table
    class="es-content"
    cellspacing="0"
    cellpadding="0"
    align="center"
    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"
>
    <tr style="border-collapse:collapse">
        <td align="center" style="padding:0;Margin:0">
            <table
                class="es-content-body"
                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px"
                cellspacing="0"
                cellpadding="0"
                align="center"
            >
                <tr style="border-collapse:collapse">
                    <td align="left" style="padding:0;Margin:0">
                        <table
                            width="100%"
                            cellspacing="0"
                            cellpadding="0"
                            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"
                        >
                            <tr style="border-collapse:collapse">
                                <td
                                    valign="top"
                                    align="center"
                                    style="padding:0;Margin:0;width:600px"
                                >
                                    <table
                                        style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;background-color:#FFECD1;border-radius:4px"
                                        width="100%"
                                        cellspacing="0"
                                        cellpadding="0"
                                        bgcolor="#ffecd1"
                                        role="presentation"
                                    >
                                        <tr style="border-collapse:collapse">
                                            <td
                                                align="center"
                                                style="padding:0;Margin:0;padding-top:30px;padding-left:30px;padding-right:30px"
                                            >
                                                <h3
                                                    style="Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#111111"
                                                >
                                                    Need more help?
                                                </h3>
                                            </td>
                                        </tr>
                                        <tr style="border-collapse:collapse">
                                            <td
                                                esdev-links-color="#ffa73b"
                                                align="center"
                                                style="padding:0;Margin:0;padding-bottom:30px;padding-left:30px;padding-right:30px"
                                            >
                                                <a
                                                    target="_blank"
                                                    href="https://viewstripo.email/"
                                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;text-decoration:underline;color:#FFA73B"
                                                    >We’re here, ready to
                                                    talk</a
                                                >
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
