<!doctype html>
<html class="no-js " lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/png">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('css/dashboard/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/light/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/authentication.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/color_skins.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/app.css')}}"> 
    <style>
        .authentication .card-plain .logo-container{
            width: 150px;
        }
    </style>
</head>
<body class="theme-purple authentication sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
        <div class="container">        
            <div class="navbar-translate n_logo">
                <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank"></a>
                <button class="navbar-toggler" type="button">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="{{route('index')}}">Home</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" title="Follow us on Twitter" href="javascript:void(0);" target="_blank">
                            <i class="zmdi zmdi-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="Like us on Facebook" href="javascript:void(0);" target="_blank">
                            <i class="zmdi zmdi-facebook"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="Follow us on Instagram" href="javascript:void(0);" target="_blank">                        
                            <i class="zmdi zmdi-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li>  --}}
                    @if (Route::is('login'))
                    
                    <li class="nav-item">
                        <a class="nav-link btn btn-white btn-round" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                    
                    @else
                     
                    <li class="nav-item">
                        <a class="nav-link btn btn-white btn-round" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                     
                    @endif               
                    
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="page-header">
        <div class="page-header-image" style="background-image:url({{asset('img/banner/slide1.jpg')}})"></div>
        @yield('content')
        <footer class="footer">
            <div class="container">
                <div class="copyright">
                    @include('layouts.assets.copyright')
                </div>
            </div>
        </footer>
    </div>
    <script src="{{asset('js/dashboard/libscripts.bundle.js')}}"></script>
    <script src="{{asset('js/dashboard/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>