@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/aseguradora.list')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="card">
                    <div class="body">
                        @empty($aseguradora)
                        <ul class="nav nav-tabs padding-0">
                            
                            
                                <span class="mx-auto text-center">
                                    <i class="zmdi zmdi-alert-octagon text-primary" style="font-size: 32px"></i> 
                                    <br>
                                     @lang('custom/dashboard/aseguradora.index.not_agency')
                                </span> 
                            
                        </ul>
                        @else
                        <div class="tab-content m-t-10">
                            @if (Route::is('dashboard.aseguradora.show'))
                            <div class="tab-pane active">
                                <dashboard-agency-admin-list-admin :t="'{{Cookie::get('template')}}'" :a="{{$aseguradora}}" :lang="{{$lang}}" v-on:select-agency="selectComplete"></dashboard-agency-admin-list-admin>
                            </div>
                            @endif
                            @if (Route::is('dashboard.aseguradora.show.polizas'))
                            <div class="tab-pane active">
                                <dashboard-agency-admin-list-agent :t="'{{Cookie::get('template')}}'" :a="{{$aseguradora}}" :lang="{{$lang}}" v-on:select-agency="selectComplete"></dashboard-agency-admin-list-agent>
                            </div>
                            @endif
                            @if (Route::is('dashboard.aseguradora.show.matriz'))
                            <div class="tab-pane active">
                                <dashboard-agency-admin-list-matriz :t="'{{Cookie::get('template')}}'" :a="{{$aseguradora}}" :lang="{{$lang}}" v-on:select-agency="selectComplete"></dashboard-agency-admin-list-matriz>
                            </div>
                            @endif    
                        </div>
                        @endempty
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>

@endsection