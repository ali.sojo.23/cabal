@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/aseguradora.index')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <ul class="nav nav-tabs padding-0">
                            @empty($aseguradora)
                                <span class="mx-auto text-center">
                                    <i class="zmdi zmdi-alert-octagon text-primary" style="font-size: 32px"></i> 
                                    <br>
                                     @lang('custom/dashboard/agency.index.not_agency')
                                </span>
                            @endempty
                        </ul>
                    </div>
                </div>
                <div class="tab-content m-t-10">
                    
                    @empty($aseguradora)
                    @else
                    <div class="tab-pane active">
                        <dashboard-agency-index-tab :t="'{{Cookie::get('template')}}'" :lang="{{$lang}}" :aseguradora="{{$aseguradora}}"></dashboard-agency-index-tab>
                    </div>    
                    @endempty
                </div>
            </div>
        </div>
    </div>

@endsection