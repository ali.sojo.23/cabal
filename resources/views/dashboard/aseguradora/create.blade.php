@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/aseguradora.create.formwizard')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    
                    <div class="body">
                        <dashboard-agency-create-form-wizard :lang="{{ $lang }}"></dashboard-agency-create-form-wizard>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection