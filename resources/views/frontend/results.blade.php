<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Title -->
    <title>{{ $title }} - {{ config('app.name', 'Laravel') }}</title>
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/png">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSS Template -->
    <link href="static/css/style.css" rel="stylesheet">
</head>

<body>
    <div id="app">
    <!-- Skippy -->
    <a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
        <div class="container">
            <span class="u-skiplink-text">Skip to main content</span>
        </div>
    </a>
    <!-- End Skippy -->
    <!-- Preload -->
    <div id="loading" class="preloader">
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- End Preload -->

    <!-- Header -->
    <header class="header-main header-dark fixed-top header-fluid">
        <!-- End Top Header -->
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <!-- Logo -->
                <a class="navbar-brand" href="index.html">
                    <img class="logo-dark" src="{{asset('img/logo.png')}}" title="" alt="">
                    <img class="logo-light" src="{{asset('img/logo_white.png')}}" title="" alt="">
                </a>
                <!-- Logo -->
                <!-- Mobile Toggle -->
                <button class="navbar-toggler ms-auto me-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- End Mobile Toggle -->
                <!-- Menu -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <frontend-header-menu></frontend-header-menu>
                </div>
            </div>
        </nav>
    </header>
    <!-- Header End -->
    <!-- Main -->
    <main>
        <!-- Home Banner -->
        <section id="home" class="py-10 effect-section skew-effect primary-after bg-gray-100">
            <div class="white-shade-effect"></div>
            <div class="container position-relative py-lg-12 z-index-9">
                <div class="row justify-content-center py-8">
                    <div class="col-lg-10 col-xl-8 text-center text-white pb-5">
                        <h1 class="display-5 pb-2">¡PROTEGE TU FAMILIA!</h1>
                        <div class="lead w-80 mx-auto mb-6">ASEGÚRATE EN DÓLARES Y OBTÉN GARANTÍA Y SEGURIDAD CON UNA COBERTURA REAL. PERMITE CUBRIR CUALQUIER EVENTO DE SALUD EN VENEZUELA, CON BENEFICIOS ÚNICOS EN EL MERCADO NACIONAL.</div>
                        <div class="px-3 py-2 bg-white rounded shadow-style">
                            <frontend-form-query></frontend-form-query>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Home Banner -->
        {{-- <!-- Section -->
        <section class="section pt-0 bg-gray-100 border-bottom">
            <div class="container mt-n12">
                <div class="row">
                    <div class="col-lg-4 my-3">
                        <div class="py-5 px-3 shadow shadow-hover rounded-3 hover-top text-center bg-white rounded-style">
                            <div class="icon icon-xl bg-white shadow rounded text-primary mb-3">
                                <i class="bi bi-clock-history"></i>
                            </div>
                            <h5 class="pt-3 h5 mb-3">Time Saving</h5>
                            <p class="mx-auto w-lg-95 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-primary" href="#">
                                <span class="btn--text">Get Start today</span>
                                <i class="bi bi-arrow-up-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="py-5 px-3 shadow shadow-hover rounded-3 hover-top text-center bg-white rounded-style">
                            <div class="icon icon-xl bg-white shadow rounded text-primary mb-3">
                                <i class="bi bi-person-lines-fill"></i>
                            </div>
                            <h5 class="pt-3 h5 mb-3">Collaboration</h5>
                            <p class="mx-auto w-lg-95 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-primary" href="#">
                                <span class="btn--text">Get Start today</span>
                                <i class="bi bi-arrow-up-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="py-5 px-3 shadow shadow-hover rounded-3 hover-top text-center bg-white rounded-style">
                            <div class="icon icon-xl bg-white shadow rounded text-primary mb-3">
                                <i class="bi bi-bootstrap-fill"></i>
                            </div>
                            <h5 class="pt-3 h5 mb-3">Bootstrap 5</h5>
                            <p class="mx-auto w-lg-95 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a class="btn btn-primary" href="#">
                                <span class="btn--text">Get Start today</span>
                                <i class="bi bi-arrow-up-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section --> --}}
    
        <section id="price" class="section bg-gray-100">
            <div class="container">
                <div class="row justify-content-center section-heading">
                    <div class="col-lg-8 col-xl-7 text-center">
                        <h3 class="display-5 after-50 primary-after pb-3 mb-4">A solo pocos clicks de obtener tu cotización</h3>
                        <div class="lead">Selecciona la cobertura que más te atraiga y haz click en el botón "Iniciar Trámites de Compra".
                            Uno de nuestros asesores se comunicará contigo para apoyarte durante el proceso y aclarar tus inquietudes.</div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-sm-6 col-lg-3 my-3">
                        <div class="card hover-top shadow-hover">
                            <div class="card-body py-5">
                                <span class="bg-warning-light text-warning px-3 py-1 rounded">Basic Account</span>
                                <h5 class="display-6 mt-3">$49.00</h5>
                                
                                
                                <a class="btn btn-primary w-100" href="#">
                                    Seleccionar
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 my-3">
                        <div class="card hover-top shadow-hover">
                            <div class="card-body py-5">
                                <span class="bg-warning-light text-warning px-3 py-1 rounded">Basic Account</span>
                                <h5 class="display-6 mt-3">$49.00</h5>
                                
                                <a class="btn btn-primary w-100" href="#">
                                    Seleccionar
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 my-3">
                        <div class="card hover-top shadow-hover">
                            <div class="card-body py-5">
                                <span class="bg-warning-light text-warning px-3 py-1 rounded">Basic Account</span>
                                <h5 class="display-6 mt-3">$49.00</h5>
                                
                                
                                <a class="btn btn-primary w-100" href="#">
                                    Seleccionar
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 my-3">
                        <div class="card hover-top shadow-hover">
                            <div class="card-body py-5">
                                <span class="bg-warning-light text-warning px-3 py-1 rounded">Basic Account</span>
                                <h5 class="display-6 mt-3">$49.00</h5>
                                
                                
                                <a class="btn btn-primary w-100" href="#">
                                    Seleccionar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section -->
        {{-- <!-- Section -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center section-heading mb-3">
                    <div class="col-lg-8 col-xl-7 text-center">
                        <h3 class="display-5 after-50 primary-after pb-3 mb-4">Our Testimonials</h3>
                        <div class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</div>
                    </div>
                </div>
                <div class="owl-carousel px-lg-12" data-items="1" data-nav-dots="true" data-lg-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="30" data-autoplay="true">
                    <div class="p-3 shadow m-4 bg-white">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-6 col-xl-5">
                                <div class="icon icon-xl rounded-circle icon-primary mb-4">
                                    <i class="bi bi-chat-right-quote"></i>
                                </div>
                                <p class="fs-5">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="pt-3">
                                    <h6>Nancy Bayers</h6>
                                    <span>Co-founder of pxdraft</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 text-center">
                                <img src="static/img/testimonial-1.png" title="" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="p-3 shadow m-4 bg-white">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-6 col-xl-5">
                                <div class="icon icon-xl rounded-circle icon-primary mb-4">
                                    <i class="bi bi-chat-right-quote"></i>
                                </div>
                                <p class="fs-5">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="pt-3">
                                    <h6>Nancy Bayers</h6>
                                    <span>Co-founder of pxdraft</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-5 text-center">
                                <img src="static/img/testimonial-2.png" title="" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section --> --}}
    </main>
    <!-- End Main -->
    <!-- Footer -->
    <footer class="bg-gray-100 footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-center my-4">
                        <img src="{{asset('img/logo.png')}}" title="" alt="">
                        <p class="mt-3 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                        <div class="nav mx-auto justify-content-center pt-1">
                            <a class="icon icon-sm icon-primary rounded me-2" href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a class="icon icon-sm icon-primary rounded me-2" href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a class="icon icon-sm icon-primary rounded me-2" href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a class="icon icon-sm icon-primary rounded me-2" href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </div>
                <div class="col-md-2"></div>
                    <div class="col-md-4 my-4">
                        <h5 class="mb-5">Contact Us</h5>
                        <ul class="list-unstyled dark-link footer-links">
                            <li>
                                <a href="#" class="media">
                                    <div class="only-icon only-icon-sm rounded-circle">
                                        <i class="bi bi-map"></i>
                                    </div>
                                    <span class="media-body ps-3">
                                        301 The Greenhouse, <br>Custard Factory, LD, E2 8DY
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="media align-items-center">
                                    <div class="only-icon only-icon-sm rounded-circle">
                                        <i class="bi bi-envelope"></i>
                                    </div>
                                    <span class="media-body ps-3">
                                        info@domain.com
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="media align-items-center">
                                    <div class="only-icon only-icon-sm rounded-circle">
                                        <i class="bi bi-phone"></i>
                                    </div>
                                    <span class="media-body ps-3">
                                        1-800-222-000
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom border-top py-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 my-1">
                        <ul class="nav justify-content-center justify-content-md-start dark-link small footer-link-1">
                            <li class="me-3"><a href="#">Privace &amp; Policy</a></li>
                            <li class="me-3"><a href="#">Faq's</a></li>
                            <li class="me-3"><a href="#">Get a Quote</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 my-1 text-center text-md-end small">
                        <p class="m-0">© 2021 copyright <a href="pxdraft.com" class="text-reset">pxdraft</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
    </div>
    <!-- jquery -->
    <script src="static/js/jquery-3.5.1.min.js"></script>
    <!-- appear -->
    <script src="static/vendor/appear/jquery.appear.js"></script>
    <!--bootstrap-->
    <script src="static/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jarallax -->
    <script src="static/vendor/jarallax/jarallax-all.js"></script>
    
    <!-- Theme Js -->
    <script src="{{asset('js/app.js?version='.env('APP_VERSION'))}}"></script>
</body>
<!-- end body -->

</html>