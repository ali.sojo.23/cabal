<?php
return [
    "index" => "INICIO",
    "dashboard" => "Dashboard",
    "administration" => "ADMINISTRATION",
    "admin" => [
        "users" => "Usuarios",
        "users_admin" => "Usuarios Administrativos",
        "aseguradora" => "Aseguradoras",
        "aseguradora_index" => "Lista de aseguradoras",
        "aseguradora_create" => "Crear nueva aseguradora",
        "rangos" => "Rangos personalizados",
        "show_ranges" => "Rangos de edades",
        "create_ranges" => "Polizas disponibles",
        "aseguradora_config" => "CONFIGURAR TABLAS",
        "aseguradora_admin" => "Administración de precios",
        "set_price" => "Configurar precios",
    ],

    "right_sidebar" => [
        "template" => "Plantilla",
        "left_menu_template" => "Menu izquierdo",
        "languaje" => "Configurar idioma",
        "lang" => [
            "english" => "Ingles",
            "spanish" => "Español",
            "portuguese" => "Portugués"
        ],
        "light" => "Claro",
        "dark" => "Oscuro"
    ]

];
