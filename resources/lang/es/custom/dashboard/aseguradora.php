<?php
return [
    "non_selected" => [
        "agency_title" => "aseguradora",
        "agency_non_selected" => "Debe seleccionar primero una aseguradora desde la barra superior o haciendo <a class='d-inline' href='" . route('dashboard.index') . "' > click aquí </a>.",
        "agency_select" => "Seleccione una aseguradora ..."
    ],
    "breadcrumb" => [
        "agencies" => "aseguradoras",
        "create" => "Crear Nueva",
        "list" => "aseguradoras registradas",
        "set_admin" => "Asignar administrador",
        "set_agent" => "Asignar agentes",

    ],
    "title" => "Administración de aseguradoras",
    "index" => [
        "not_agency" => "No se ha agregado aseguradoras",
        "subtitle" => "Lista de aseguradoras inmobiliarias registradas.",
        "see_agency" => "Ver aseguradora"
    ],
    "show" => [
        "admin" => "Administradores",
        "agent" => "Agentes",
        "edit" => [
            "logo" => "Editar logo",
            "description" => "Editar descripción",
        ],
        "dropzone" => [
            "title" => "¡Arrastra y suelta para subir contenido!",
            "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"

        ],
        "message" => [
            "success" => [
                "updated" => "aseguradora actualizado satisfactoriamente.",
            ],
        ]
    ],
    "list" => [
        "subtitle_admin" => "Asignar usuario administrador a las aseguradoras disponibles",
        "subtitle_agent" => "Asignar agente a las aseguradoras disponibles",
        "tab1" => [
            "create_user" => "¿Crear usuario?",
            "search_user" => "¿Buscar usuario?",
            "title" => "Administrador de la aseguradora",
            "firstname" => "Nombre",
            "lastname" => "Apellido",
            "img_profile" => "Imagen de Perfil",
            "complete_info" => "Completar los siguientes datos",
            "email" => "Correo electrónico",
            "search" => "Buscar",
            "create" => "Crear Usuario",
            "cancel" => "Cancelar"
        ],
        "user_created" => "Usuario creado y asignado a la aseguradora",
        "user_assigned" => "Usuario asignado a la aseguradora",
        "see_agency" => "Ver perfil",
        "user_found" => "Usuario existente"
    ],
    "create" => [
        "subtitle" => "Agregar nuevas aseguradoras",
        "formwizard" => [
            "title" => "Nueva aseguradora",
            "subtitle" => "COMPLETE EL SIGUIENTE FORMULARIO PARA AGREGAR NUEVAS ASEGURADORAS",
            "tab2" => [
                "title" => "Datos de la aseguradora",
                "logo" => "Logo de la aseguradora",
                "name" => "Nombre de la aseguradora"
            ],
            "tab4" => [
                "title" => "Finalizar",
                "msg1" => "¡Se ha creado la aseguradora con exito!",
                "see_agency" => "VER aseguradora"
            ],
            "button" => [
                "next" => "Siguiente",
                "back" => "Regresar",
                "done" => "Hecho"
            ],
            "dropzone" => [
                "title" => "¡Arrastra y suelta para subir contenido!",
                "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"

            ],
            "messages" => [
                "required" => "Campo requerido",
                "email" => "Debe ingresar un campo valido de tipo email",
                "select_user" => "Debe seleccionar o crear un usuario",
                "complete_required" => "Debe completar todos los campos requeridos",
                "invalid_number" => "No es un número de telefono válido",
                "error" => [
                    "not_user" => "El usuario asignado a la cuenta de la aseguradora es incorreto, verifiquelo e intente nuevo."
                ]
            ]
        ]
    ],

];
