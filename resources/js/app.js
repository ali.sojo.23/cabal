/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.axios = require("axios");
/**
 * VUE JS
 */
window.Vue = require("vue");

import * as VueGoogleMaps from "vue2-google-maps";
import PhotoSwipe from "vue-photoswipe.js";
import "vue-photoswipe.js/dist/static/css/photoswipe.css";
import VueLazyload from "vue-lazyload";
import vSelect from "vue-select";
import VueFormWizard from "vue-form-wizard";
import "vue-form-wizard/dist/vue-form-wizard.min.css";
import Vue from "vue";
import Vuelidate from "vuelidate";

Vue.use(Vuelidate);
Vue.use(VueFormWizard);
Vue.component("v-select", vSelect);
Vue.use(VueLazyload, {
    preLoad: 1.3,

    loading: "/img/spacer.png",
    attempt: 1
});

Vue.use(VueGoogleMaps, {
    load: {
        key: process.env.MIX_GOOGLE_MAPS_API_KEY
        // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then set installComponents to 'false'.
    //// If you want to automatically install all the components this property must be set to 'true':
    installComponents: true
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Dashboard
// En esta sección agregagaremos los componentes que se utilizarán en
// La sección administrativa o Dashboard
Vue.component(
    "dashboard-agency-commons-search-agency",
    require("./components/dashboard/commons/searchAgency.vue").default
);
Vue.component(
    "dashboard-user-datatable",
    require("./components/dashboard/users/UsersDatatable.vue").default
);
Vue.component(
    "dashboard-user-show-profile-card",
    require("./components/dashboard/profile/ProfileCard.vue").default
);
Vue.component(
    "dashboard-user-show-reset-password",
    require("./components/dashboard/profile/ResetPassword.vue").default
);
Vue.component(
    "dashboard-user-show-edit-information",
    require("./components/dashboard/profile/EditInfoAccount.vue").default
);
Vue.component(
    "dashboard-user-show-verification-user",
    require("./components/dashboard/profile/VerificationUser.vue").default
);
Vue.component(
    "dashboard-agency-create-form-wizard",
    require("./components/dashboard/agency/create.vue").default
);
Vue.component(
    "dashboard-agency-memberships-create",
    require("./components/dashboard/memberships/create.vue").default
);
Vue.component(
    "dashboard-agency-memberships-index",
    require("./components/dashboard/memberships/index.vue").default
);
Vue.component(
    "dashboard-agency-index-tab",
    require("./components/dashboard/agency/index.vue").default
);
Vue.component(
    "dashboard-agency-show-edit-info",
    require("./components/dashboard/agency/show/edit.vue").default
);

Vue.component(
    "dashboard-agency-admin-list-admin",
    require("./components/dashboard/agency/list/admin.vue").default
);
Vue.component(
    "dashboard-agency-admin-list-agent",
    require("./components/dashboard/agency/list/agent.vue").default
);
Vue.component(
    "dashboard-agency-admin-list-matriz",
    require("./components/dashboard/agency/list/matriz.vue").default
);
Vue.component(
    "dashboard-agency-properties-create",
    require("./components/dashboard/properties/create.vue").default
);

/**
 * Next, we will create a custom frontend components
 */
Vue.component('frontend-header-menu',require('./components/frontend/header/menu.vue').default)
Vue.component('frontend-form-query',require('./components/frontend/formulario/query.vue').default)

/**
 * Next, we will create a new filers used to format document
 */
Vue.filter("SetIdForm", function(id) {
    if (id < 10) {
        return "000" + id;
    } else if (id < 100) {
        return "00" + id;
    } else if (id < 1000) {
        return "0" + id;
    } else id;
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

if (document.getElementById("main")) {
    const app = new Vue({
        el: "#main",
        created() {
            this.axiosConfig();
        },
        mounted() {
            this.setActiveMenu();
            this.verifySelectAgency();
        },
        data() {
            return {
                agencySelected: null,
                auth_token: null,
                dashboard_url: "/dashboard"
            };
        },
        methods: {
            axiosConfig() {
                var output = {};
                document.cookie.split(/\s*;\s*/).forEach(pair => {
                    pair = pair.split(/\s*=\s*/);
                    output[pair[0]] = pair.splice(1).join("=");
                });

                this.auth_token = output.auth_token;
                if (!this.auth_token) {
                    this.openModal();
                }
                localStorage.setItem("req_token", output.auth_token);
                axios.defaults.headers.common[
                    "Authorization"
                ] = output.auth_token
                    ? "Bearer " +
                      decodeURIComponent(localStorage.getItem("req_token"))
                    : "";
            },
            openModal() {
                let path = location.pathname;
                if (path.includes(this.dashboard_url)) {
                    $("#defaultModal").modal("show");
                    setTimeout(() => {
                        this.closeModal();
                    }, 300000);
                }
            },
            closeModal() {
                $("#defaultModal").modal("hide");
                this.logout();
            },
            extendSession() {
                $("#defaultModal").modal("hide");
                document.getElementById("extendSessionForm").submit();
            },
            selectComplete(e) {
                this.agencySelected = e;
            },
            verifySelectAgency() {
                this.agencySelected =
                    JSON.parse(localStorage.getItem("aseguradora_selected")) ??
                    null;
            },
            logout() {
                localStorage.removeItem("aseguradora_selected");
                document.getElementById("logout").submit();
            },
            setActiveMenu() {
                let item = document.getElementById("dashboard");
                let url = location.href;
                if (item) {
                    item = item.getElementsByClassName("menu-item");
                    for (let i of item) {
                        if (i.lastElementChild.href == url) {
                            if (i.parentElement.className == "ml-menu") {
                                i.parentElement.parentElement.className +=
                                    " active open";
                                i.className += " active";
                            } else {
                                i.className += " active open";
                            }
                        }
                    }
                }
            }
        }
    });
}

if (document.getElementById("app")) {
    const front = new Vue({
        el: "#app",
        created() {
            this.init();
        },
        mounted() {},
        methods: {
            init() {
                (function($) {
                    "use strict";
                    var CRE = {};
                    $.fn.exists = function() {
                        return this.length > 0;
                    };

                    /* ---------------------------------------------- /*
                     * Pre load
                    /* ---------------------------------------------- */
                    CRE.PreLoad = function() {
                        document.getElementById("loading").style.display =
                            "none";
                    };

                    /*--------------------
                     * Menu toogle header
                    ----------------------*/
                    CRE.MenuToggleClass = function() {
                        $(".navbar-toggler").on("click", function() {
                            var toggle = $(".navbar-toggler").is(":visible");
                            if (toggle) {
                                $("header").toggleClass("header-toggle");
                            }
                        });
                    };

                    /* ---------------------------------------------- /*
                     * Header Fixed
                    /* ---------------------------------------------- */
                    CRE.HeaderFixd = function() {
                        var HscrollTop = $(window).scrollTop();
                        var HHeight = $(".header-height").outerHeight();
                        var HHeightTop = $(".header-top").outerHeight();
                        $(".header-height-bar").css("min-height", HHeight);

                        if (HscrollTop >= 100) {
                            $(".navbar-dark").addClass("navbar-light");
                            $(".navbar-dark").addClass("navbar-dark-top");
                            $(".navbar-dark-top").removeClass("navbar-dark");
                            $(".header-main").addClass("fixed-header");
                            $(".header-main").css("top", -HHeightTop);
                        } else {
                            $(".navbar-dark-top").removeClass("navbar-light");
                            $(".navbar-dark-top").addClass("navbar-dark");
                            $(".navbar-dark").removeClass("navbar-dark-top");
                            $(".header-main").removeClass("fixed-header");
                            $(".header-main").css("top", 0);
                        }
                    };

                    /* ---------------------------------------------- /*
                     * Header height
                    /* ---------------------------------------------- */
                    CRE.HeaderHeight = function() {
                        /*var HHeight = $('.header-height').outerHeight()
                            var HHeightTop = $('.header-top').outerHeight()
                        $('.header-height-bar').css("min-height", HHeight);
                        $('.header-main').css("top", - HHeightTop);*/
                    };

                    /* ---------------------------------------------- /*
                     * Mega Menu
                    /* ---------------------------------------------- */

                    CRE.MegaMenu = function() {
                        var mDropdown = $(".px-dropdown-toggle");
                        mDropdown.on("click", function() {
                            $(this)
                                .parent()
                                .toggleClass("open-menu-parent");
                            $(this)
                                .next(".dropdown-menu")
                                .toggleClass("show");
                            $(this).toggleClass("open");
                        });
                    };

                    /*--------------------
                     * Counter
                    ----------------------*/
                    CRE.Counter = function() {
                        //var counter = jQuery(".counter");
                        var $counter = $(".counter");
                        if ($counter.length > 0) {
                            $counter.each(function() {
                                var $elem = $(this);
                                $elem.appear(function() {
                                    $elem.find(".count").countTo({
                                        speed: 2000,
                                        refreshInterval: 10
                                    });
                                });
                            });
                        }
                    };

                    /*--------------------
                     * Typed
                    ----------------------*/
                    CRE.typedbox = function() {
                        var typedjs = $(".typed");
                        if (typedjs.length > 0) {
                            typedjs.each(function() {
                                var $this = $(this);
                                $this.typed({
                                    strings: $this
                                        .attr("data-elements")
                                        .split(","),
                                    typeSpeed: 150, // typing speed
                                    backDelay: 500 // pause before backspacing
                                });
                            });
                        }
                    };

                    /*--------------------
                     * Owl Corousel
                    ----------------------*/
                    CRE.Owl = function() {
                        var owlslider = $("div.owl-carousel");
                        if (owlslider.length > 0) {
                            owlslider.each(function() {
                                var $this = $(this),
                                    $items = $this.data("items")
                                        ? $this.data("items")
                                        : 1,
                                    $loop = $this.attr("data-loop")
                                        ? $this.data("loop")
                                        : true,
                                    $navdots = $this.data("nav-dots")
                                        ? $this.data("nav-dots")
                                        : false,
                                    $navarrow = $this.data("nav-arrow")
                                        ? $this.data("nav-arrow")
                                        : false,
                                    $autoplay = $this.attr("data-autoplay")
                                        ? $this.data("autoplay")
                                        : true,
                                    $autospeed = $this.attr("data-autospeed")
                                        ? $this.data("autospeed")
                                        : 5000,
                                    $smartspeed = $this.attr("data-smartspeed")
                                        ? $this.data("smartspeed")
                                        : 1000,
                                    $autohgt = $this.data("autoheight")
                                        ? $this.data("autoheight")
                                        : false,
                                    $CenterSlider = $this.data("center")
                                        ? $this.data("center")
                                        : false,
                                    $stage = $this.attr("data-stage")
                                        ? $this.data("stage")
                                        : 0,
                                    $space = $this.attr("data-space")
                                        ? $this.data("space")
                                        : 30;

                                $(this).owlCarousel({
                                    loop: $loop,
                                    items: $items,
                                    responsive: {
                                        0: {
                                            items: $this.data("xs-items")
                                                ? $this.data("xs-items")
                                                : 1
                                        },
                                        576: {
                                            items: $this.data("sm-items")
                                                ? $this.data("sm-items")
                                                : 1
                                        },
                                        768: {
                                            items: $this.data("md-items")
                                                ? $this.data("md-items")
                                                : 1
                                        },
                                        992: {
                                            items: $this.data("lg-items")
                                                ? $this.data("lg-items")
                                                : 1
                                        },
                                        1200: {
                                            items: $items
                                        }
                                    },
                                    dots: $navdots,
                                    autoplayTimeout: $autospeed,
                                    smartSpeed: $smartspeed,
                                    autoHeight: $autohgt,
                                    center: $CenterSlider,
                                    margin: $space,
                                    stagePadding: $stage,
                                    nav: $navarrow,
                                    navText: [
                                        "<i class='ti-angle-left'></i>",
                                        "<i class='ti-angle-right'></i>"
                                    ],
                                    autoplay: $autoplay,
                                    autoplayHoverPause: true
                                });
                            });
                        }
                    };

                    /* ---------------------------------------------- /*
                        * lightbox gallery
                    /* ---------------------------------------------- */
                    CRE.Gallery = function() {
                        var GalleryPopup = $(".lightbox-gallery");
                        if (GalleryPopup.length > 0) {
                            $(".lightbox-gallery").magnificPopup({
                                delegate: ".gallery-link",
                                type: "image",
                                tLoading: "Loading image #%curr%...",
                                mainClass: "mfp-fade",
                                fixedContentPos: true,
                                closeBtnInside: false,
                                gallery: {
                                    enabled: true,
                                    navigateByImgClick: true,
                                    preload: [0, 1] // Will preload 0 - before current, and 1 after CRE current image
                                }
                            });
                        }
                        var VideoPopup = $(".video-btn");
                        if (VideoPopup.length > 0) {
                            $(".video-btn").magnificPopup({
                                disableOn: 700,
                                type: "iframe",
                                mainClass: "mfp-fade",
                                removalDelay: 160,
                                preloader: false,
                                fixedContentPos: false
                            });
                        }
                    };

                    /*--------------------
                     * Masonry
                    ----------------------*/
                    CRE.masonry = function() {
                        var portfolioWork = $(".portfolio-content");
                        if (portfolioWork.length > 0) {
                            $(portfolioWork).isotope({
                                resizable: false,
                                itemSelector: ".grid-item",
                                layoutMode: "masonry",
                                filter: "*"
                            });
                            //Filtering items on portfolio.html
                            var portfolioFilter = $(".filter li");
                            // filter items on button click
                            $(portfolioFilter).on("click", function() {
                                var filterValue = $(this).attr("data-filter");
                                portfolioWork.isotope({
                                    filter: filterValue
                                });
                            });
                            //Add/remove class on filter list
                            $(portfolioFilter).on("click", function() {
                                $(this)
                                    .addClass("active")
                                    .siblings()
                                    .removeClass("active");
                            });
                        }
                    };

                    /*--------------------
                     * Progress Bar 
                    ----------------------*/
                    CRE.ProgressBar = function() {
                        $(".skill-bar .skill-bar-in").each(function() {
                            var bottom_object =
                                $(this).offset().top + $(this).outerHeight();
                            var bottom_window =
                                $(window).scrollTop() + $(window).height();
                            var progressWidth =
                                $(this).attr("aria-valuenow") + "%";
                            if (bottom_window > bottom_object) {
                                $(this).css({
                                    width: progressWidth
                                });
                            }
                        });
                    };
                    /*-----------------------
                     * SVG
                    -------------------------*/
                    var mySVGsToInject = document.querySelectorAll(
                        ".svg_img, .svg_icon"
                    );
                    CRE.SVGbx = function() {
                        if (mySVGsToInject.length > 0) {
                            SVGInjector(mySVGsToInject);
                        }
                    };

                    /*--------------------
                     * pieChart
                    ----------------------*/
                    CRE.pieChart = function() {
                        var $Pie_Chart = $(".pie_chart_in");
                        if ($Pie_Chart.length > 0) {
                            $Pie_Chart.each(function() {
                                var $elem = $(this),
                                    pie_chart_size =
                                        $elem.attr("data-size") || "160",
                                    pie_chart_animate =
                                        $elem.attr("data-animate") || "2000",
                                    pie_chart_width =
                                        $elem.attr("data-width") || "6",
                                    pie_chart_color =
                                        $elem.attr("data-color") || "#84ba3f",
                                    pie_chart_track_color =
                                        $elem.attr("data-trackcolor") ||
                                        "rgba(0,0,0,0.10)",
                                    pie_chart_line_Cap =
                                        $elem.attr("data-lineCap") || "round",
                                    pie_chart_scale_Color =
                                        $elem.attr("data-scaleColor") || "true";
                                $elem.find("span, i").css({
                                    width: pie_chart_size + "px",
                                    height: pie_chart_size + "px",
                                    "line-height": pie_chart_size + "px",
                                    position: "absolute"
                                });
                                $elem.appear(function() {
                                    $elem.easyPieChart({
                                        size: Number(pie_chart_size),
                                        animate: Number(pie_chart_animate),
                                        trackColor: pie_chart_track_color,
                                        lineWidth: Number(pie_chart_width),
                                        barColor: pie_chart_color,
                                        scaleColor: false,
                                        lineCap: pie_chart_line_Cap,
                                        onStep: function(from, to, percent) {
                                            $elem
                                                .find("span.middle")
                                                .text(Math.round(percent));
                                        }
                                    });
                                });
                            });
                        }
                    };

                    /*--------------------
                     * Parallax
                    ----------------------*/
                    CRE.parallax = function() {
                        var Parallax = $(".parallax");
                        if (Parallax.length > 0) {
                            jarallax(document.querySelectorAll(".parallax"));
                            /*jarallax(document.querySelectorAll('.parallax-img'), {
                    keepImg: true,
                });*/
                        }
                    };

                    // Window on Load
                    $(window).on("load", function() {
                        CRE.masonry(), CRE.PreLoad();
                    });
                    // Document on Ready
                    $(document).ready(function() {
                        CRE.SVGbx(),
                            CRE.HeaderFixd(),
                            CRE.Counter(),
                            CRE.MenuToggleClass(),
                            CRE.Gallery(),
                            CRE.HeaderHeight(),
                            CRE.ProgressBar(),
                            CRE.parallax(),
                            CRE.MegaMenu(),
                            CRE.typedbox(),
                            CRE.pieChart(),
                            CRE.Owl();
                    });

                    // Document on Scrool
                    $(window).scroll(function() {
                        CRE.ProgressBar(), CRE.HeaderFixd();
                    });

                    // Window on Resize
                    $(window).resize(function() {
                        CRE.HeaderHeight();
                    });
                })(jQuery);
            }
        }
    });
}
